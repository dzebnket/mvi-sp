1 Step
Importing the necessary packages and changing the accelerator to TPU, because GPUs are typically less efficient in the way they work with neural networks than a TPU. TPUs are more specialized for machine learning calculations.
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
2 Step 
Loading the data: we should keep our photo dataset and Monet dataset separately. All the images have to be the right size and also we should scale them to a [-1,1] scale. As we build a GAN, we don't need the labels or ids of them, so we will return only image itself from the TFRecord. For this pursposes we should define a function for extracting the image from the file. 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
3 Step 
Building the generator: At first we will try UNET architecture for CycleGAN. We will apply two methods
a) downsample, which reduces the 2d dimensions (width and height) by the stride (the length of the step our filter will take). For example, if the stride is 2, the filter is applied to every other pixel, reducing the weight and height by 2. 
Cyclegan uses instance normalization instead of batch normalization  and we will use the layer from TensorFlow Add-ons (Conv2D layer).
b) upsample is the opposite method. It increases the dimensions of the image (Conv2DTranspose will be used). 

At first our generator will downsample the input images and then upsample, establishing long skip connections.Then we will concatenate the output of the downsample layer to the upsample layer in a symmetrical way. So there will be 2 generators:
monet_generator, which transforms photos to Monet-esque paintings
photo_generator, which transforms Monet paintings to be more like photos
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
4 Step
Building the discriminator: It will classify if the image is real or generated. Our discriminator on the output will give us a smaller 2d image with higher pixel values indicating if it's real and lower values if it's fake. So there will be 2 discriminators:
monet_discriminator, which differentiates real Monet paintings and generated Monet paintings
photo_discriminator, which differentiates real photos and generated photos
For the better result our generators should be trained. 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
5 Step 
Building the CycleGAN model: subclassing the tf.keras.Model for running fit() to train it. While training, the model will transform a photo to a Monet painting and then back to a photo. The difference between the original photo and twice-transformed will be our cycle-consistency loss. The aim is to make the original photo and twice-transformed similar to one another. Cycle consistency means the result should be close to the original input.
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
6 Step 
Defining the loss function: In this case we will use tf.keras.losses.BinaryCrossentropy. 
The discriminator loss function compares real images to a matrix of 1s and fake of 0s. If it works perfectly it should give us all 1s for real images and all 0s for fake. 
The discriminator loss will output the average of the real and generated loss.
Initialize the optimizers for all the generators and the discriminators.
The generator wants to make our discriminator think that generated image is real so it should compare the generated image to a matrix of 1s to find the loss. 
As we already mentioned we want to make our original image and the twice-transformed to be similar to each other, so we can calculate the cycle consistency loss as the average of their difference. The identity loss will compare the image with its generator. 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
7 Step 
Training the model using fit() 
Instead of it we can also follow these 4 steps:
Get the predictions.
Calculate the loss.
Calculate the gradients using backpropagation.
Apply the gradients to the optimizer.
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
8 Step 
Checking the results
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
We can also implement another modified ResNet generator instead of the U-Net generator used here.